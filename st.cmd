require ipmimanager

epicsEnvSet("P", "LabS:IpmiDemo:")

# when set to 0 console will not produce message after ioc init, when set to 1 all messages will be visible
epicsEnvSet("DISP_MSG", 1)

# crate IP address
epicsEnvSet("MCH_ADDR", "172.30.5.248")
epicsEnvSet("PORT", "$(MCH_ADDR)")
epicsEnvSet("TELNET_PORT", "23")
epicsEnvSet("ARCHIVER_SIZE", "1024")

epicsEnvSet("CRATE_NUM", "1")
epicsEnvSet("MTCA_PREF", "$(P)MTCA-$(CRATE_NUM)00:")
epicsEnvSet("IOC_PREF", "$(P)IOC-$(CRATE_NUM)00:")

# Panels compatibility version
epicsEnvSet("PANEL_VER", "2.0.0")

# Common crate configuration
iocshLoad("iocsh/common-9u.iocsh")

# CCT AM G6x CPU in slot 1
#iocshLoad("iocsh/cct-am-g6x.iocsh", "IDX=01, SLOT=1")

# mTCA-EVR-300 in slot 2
iocshLoad("iocsh/mtca-evr-300.iocsh", "IDX=01, SLOT=2")

# IOxOS IFC-1410 in slot 4
iocshLoad("iocsh/ifc-1410.iocsh", "IDX=01, SLOT=4")

# IOxOS IFC-1410 in slot 5
#iocshLoad("iocsh/ifc-1410.iocsh", "IDX=02, SLOT=5")

# IOxOS IFC-1410 in slot 6
iocshLoad("iocsh/ifc-1410.iocsh", "IDX=03, SLOT=6")

# IOxOS IFC-1410 in slot 7
iocshLoad("iocsh/ifc-1410.iocsh", "IDX=04, SLOT=7")

# Generic startup commands
iocshLoad("$(ipmimanager_DIR)connect.iocsh", "TIMEOUT=10, USE_STREAM=, STREAM_PORT=$(TELNET_PORT),CHECK_VER=#,PANEL_VER=$(PANEL_VER),USE_EXPERT=")

eltc "$(DISP_MSG)"
